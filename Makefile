CC = nvcc
CFLAGS =  -gencode=arch=compute_35,code=compute_35 -rdc=true -O3
LDFLAGS = -lm

FRAMES := 10

all: c63enc c63dec c63pred

%.o: %.cu
	$(CC) $< $(CFLAGS) -c -o $@

c63enc: c63enc.o dsp.o tables.o io.o c63_write.o  common.o me.o
	$(CC) $^ $(CFLAGS) $(LDFLAGS) -o $@
c63dec: c63dec.o dsp.o tables.o io.o  common.o me.o
	$(CC) $^ $(CFLAGS) $(LDFLAGS) -o $@
c63pred: c63dec.o dsp.o tables.o io.o  common.o me.o
	$(CC) $^ -DC63_PRED $(CFLAGS) $(LDFLAGS) -o $@

.PHONY: clean distclean
clean:
	rm -f *.o c63enc c63dec c63pred

distclean:
	rm -f *.o c63enc c63dec c63pred
	rm -R output

output:
	mkdir output

.PHONY: foreman
foreman: c63enc output
	./c63enc -w 352 -h 288 -o output/foreman.c63 -f $(FRAMES) /opt/cipr/foreman.yuv

.PHONY: 4k
4k: c63enc output
	./c63enc -w 3840 -h 2160 -o output/foreman_4k.c63 -f $(FRAMES) /opt/cipr/foreman_4k.yuv

.PHONY: 4k2
4k2: c63enc output
	./c63enc -w 3840 -h 2160 -o output/foreman_4k_2.c63 -f $(FRAMES) /opt/cipr/foreman_4k.yuv

.PHONY: diff-4k
diff-4k:
	diff output/foreman_4k.c63 output/foreman_4k_2.c63

.PHONY: bagadus
bagadus: c63enc output
	./c63enc -w 4096 -h 1680 -o output/bagadus.c63 -f $(FRAMES) /opt/cipr/bagadus.yuv

.PHONY: tractor
tractor: c63enc output
	./c63enc -w 1920 -h 1080 -o output/tractor.c63 -f $(FRAMES) /opt/cipr/tractor.yuv

%.yuv : %.c63 c63dec
	-./c63dec $< $@

.PHONY: mplay-foreman
mplay-foreman: output/foreman.yuv
	mplayer output/foreman.yuv -demuxer rawvideo -rawvideo w=352:h=288

.PHONY: mplay
mplay: output/foreman_4k.yuv
	mplayer output/foreman_4k.yuv -demuxer rawvideo -rawvideo w=3840:h=2160

.PHONY: mplay-bagadus
mplay-bagadus: output/bagadus.yuv
	mplayer output/bagadus.yuv -demuxer rawvideo -rawvideo w=4096:h=1680

.PHONY: mplay-tractor
mplay-tractor: output/tractor.yuv
	mplayer output/tractor.yuv -demuxer rawvideo -rawvideo w=1920:h=1080

.PHONY: memcheck
memcheck : c63enc
	cuda-memcheck ./c63enc -w 352 -h 288 -o output/foreman.c63 -f 10 /opt/cipr/foreman.yuv

.PHONY: nvprof
nvprof : c63enc
	nvprof ./c63enc -w 3840 -h 2160 -o output/foreman_4k.c63 -f 10 /opt/cipr/foreman_4k.yuv
