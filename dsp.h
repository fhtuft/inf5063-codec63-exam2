#ifndef C63_DSP_H_
#define C63_DSP_H_

#define ISQRT2 0.70710678118654f

#include <inttypes.h>

struct sad {
  int sad;
  int x;
  int y;
};


void dct_quant_block_8x8(int16_t *in_data, int16_t *out_data,
    uint8_t *quant_tbl);

void dequant_idct_block_8x8(int16_t *in_data, int16_t *out_data,
    uint8_t *quant_tbl);

void sad_block_8x8(uint8_t *block1, uint8_t *block2, int stride, int *result);

__global__ void cuda_sad_block_8x8(uint8_t *block1, uint8_t *window, int stride, int *sad);
__device__ void dev_sad_block_8x8(uint8_t *block1, uint8_t *window, int sw, int sh, int stride, int *sad);
/*__global__ void cuda_sad_block_8x8(uint8_t *block1, uint8_t *block2,int stride,int top,int bottom,
 *                                   int left,int right,struct sad *cuda_min_sad); */
__global__ void cuda2_sad_block_8x8(uint8_t *block1, uint8_t *block2,int *result);
__global__ void cuda_dct_quant_block_8x8(uint8_t *in_data, uint8_t *pred, int16_t *out_data,
    uint8_t *quant_tbl);
__global__ void cuda_dequant_idct_block_8x8(int16_t *in_data, uint8_t *pred,
		uint8_t *out_data, uint8_t *quant_tbl);


#endif  /* C63_DSP_H_ */
