#include <assert.h>
#include <errno.h>
#include <getopt.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"
#include "dsp.h"
#include "stdcuda.h"

void dequantize_idct_row(int16_t *in_data, uint8_t *prediction, int w, int h,
    int y, uint8_t *out_data, uint8_t *quantization)
{
  int x;

  int16_t block[8*8];

  /* Perform the dequantization and iDCT */
  for(x = 0; x < w; x += 8)
  {
    int i, j;

    dequant_idct_block_8x8(in_data+(x*8), block, quantization);

    for (i = 0; i < 8; ++i)
    {
      for (j = 0; j < 8; ++j)
      {
        /* Add prediction block. Note: DCT is not precise -
           Clamp to legal values */
        int16_t tmp = block[i*8+j] + (int16_t)prediction[i*w+j+x];

        if (tmp < 0) { tmp = 0; }
        else if (tmp > 255) { tmp = 255; }

        out_data[i*w+j+x] = tmp;
      }
    }
  }
}

void cuda_dequantize_idct(int16_t *in_data, uint8_t *prediction, uint32_t width,
    uint32_t height, uint8_t *out_data, uint8_t *quantization)
{
  dim3 blocks = dim3(width/8, height/8); // one for each macro block
  dim3 threads = dim3(8,8); // one for each pixel in macro block
  cuda_dequant_idct_block_8x8<<<blocks,threads>>>(in_data, prediction, out_data, quantization);
}

void dequantize_idct(int16_t *in_data, uint8_t *prediction, uint32_t width,
    uint32_t height, uint8_t *out_data, uint8_t *quantization)
{
  int y;

  for (y = 0; y < height; y += 8)
  {
    dequantize_idct_row(in_data+y*width, prediction+y*width, width, height, y,
        out_data+y*width, quantization);
  }
}



void dct_quantize_row(uint8_t *in_data, uint8_t *prediction, int w, int h,
    int16_t *out_data, uint8_t *quantization)
{
  int x;

  int16_t block[8*8];

  /* Perform the DCT and quantization */
  for(x = 0; x < w; x += 8)
  {
    int i, j;

    for (i = 0; i < 8; ++i)
    {
      for (j = 0; j < 8; ++j)
      {
        block[i*8+j] = ((int16_t)in_data[i*w+j+x] - prediction[i*w+j+x]);
      }
    }

    /* Store MBs linear in memory, i.e. the 64 coefficients are stored
       continuous. This allows us to ignore stride in DCT/iDCT and other
       functions. */
    dct_quant_block_8x8(block, out_data+(x*8), quantization);
  }
}

void cuda_dct_quantize(uint8_t *in_data, uint8_t *prediction, uint32_t width,
    uint32_t height, int16_t *out_data, uint8_t *quantization)
{
  dim3 blocks = dim3(width/8, height/8); // one for each macro block
  dim3 threads = dim3(8,8); // one for each pixel in macro block
  cuda_dct_quant_block_8x8<<<blocks,threads>>>(in_data, prediction, out_data, quantization);
}

void dct_quantize(uint8_t *in_data, uint8_t *prediction, uint32_t width,
    uint32_t height, int16_t *out_data, uint8_t *quantization)
{
  int y;

  for (y = 0; y < height; y += 8)
  {
    dct_quantize_row(in_data+y*width, prediction+y*width, width, height,
        out_data+y*width, quantization);
  }
}

void destroy_frame(struct frame *f)
{
  /* First frame doesn't have a reconstructed frame to destroy */
  if (!f) { return; }

  CUDA_ERROR( cudaFree(f->recons->Y) );
  CUDA_ERROR( cudaFree(f->recons->U) );
  CUDA_ERROR( cudaFree(f->recons->V) );
  free(f->recons);

  CUDA_ERROR( cudaFree(f->residuals->Ydct) );
  CUDA_ERROR( cudaFree(f->residuals->Udct) );
  CUDA_ERROR( cudaFree(f->residuals->Vdct) );
  free(f->residuals);

  CUDA_ERROR( cudaFree(f->predicted->Y) );
  CUDA_ERROR( cudaFree(f->predicted->U) );
  CUDA_ERROR( cudaFree(f->predicted->V) );
  free(f->predicted);

  //free(f->mbs[Y_COMPONENT]);
  //free(f->mbs[U_COMPONENT]);
  //free(f->mbs[V_COMPONENT]);

  CUDA_ERROR( cudaFree(f->mbs[Y_COMPONENT]) );
  CUDA_ERROR( cudaFree(f->mbs[U_COMPONENT]) );
  CUDA_ERROR( cudaFree(f->mbs[V_COMPONENT]) );
  

  free(f);
}

struct frame* create_frame(struct c63_common *cm, yuv_t *image)
{
  struct frame *f = (struct frame *)malloc(sizeof(struct frame));

  f->orig = image;

  f->recons = (yuv_t *)malloc(sizeof(yuv_t));
  //f->recons->Y = (uint8_t *)malloc(cm->ypw * cm->yph);
  //f->recons->U = (uint8_t *)malloc(cm->upw * cm->uph);
  //f->recons->V = (uint8_t *)malloc(cm->vpw * cm->vph);
  CUDA_ERROR( cudaMallocManaged(&(f->recons->Y), cm->ypw * cm->yph) );
  CUDA_ERROR( cudaMallocManaged(&(f->recons->U), cm->upw * cm->uph) );
  CUDA_ERROR( cudaMallocManaged(&(f->recons->V), cm->vpw * cm->vph) );

  f->predicted = (yuv_t *)malloc(sizeof(yuv_t));
  //f->predicted->Y = (uint8_t*)calloc(cm->ypw * cm->yph, sizeof(uint8_t));
  //f->predicted->U = (uint8_t*)calloc(cm->upw * cm->uph, sizeof(uint8_t));
  //f->predicted->V = (uint8_t*)calloc(cm->vpw * cm->vph, sizeof(uint8_t));
  CUDA_ERROR( cudaMallocManaged(&(f->predicted->Y), cm->ypw * cm->yph * sizeof(uint8_t)) );
  CUDA_ERROR( cudaMallocManaged(&(f->predicted->U), cm->upw * cm->uph * sizeof(uint8_t)) );
  CUDA_ERROR( cudaMallocManaged(&(f->predicted->V), cm->vpw * cm->vph * sizeof(uint8_t)) );
  CUDA_ERROR( cudaMemset(f->predicted->Y, 0, cm->ypw * cm->yph * sizeof(uint8_t)) );
  CUDA_ERROR( cudaMemset(f->predicted->U, 0, cm->upw * cm->uph * sizeof(uint8_t)) );
  CUDA_ERROR( cudaMemset(f->predicted->V, 0, cm->vpw * cm->vph * sizeof(uint8_t)) );

  f->residuals = (dct_t*)malloc(sizeof(dct_t));
  //f->residuals->Ydct = (int16_t*)calloc(cm->ypw * cm->yph, sizeof(int16_t));
  //f->residuals->Udct = (int16_t*)calloc(cm->upw * cm->uph, sizeof(int16_t));
  //f->residuals->Vdct = (int16_t*)calloc(cm->vpw * cm->vph, sizeof(int16_t));
  CUDA_ERROR( cudaMallocManaged(&(f->residuals->Ydct), cm->ypw * cm->yph * sizeof(int16_t)) );
  CUDA_ERROR( cudaMallocManaged(&(f->residuals->Udct), cm->upw * cm->uph * sizeof(int16_t)) );
  CUDA_ERROR( cudaMallocManaged(&(f->residuals->Vdct), cm->vpw * cm->vph * sizeof(int16_t)) );
  CUDA_ERROR( cudaMemset(f->residuals->Ydct, 0,cm->ypw * cm->yph * sizeof(int16_t)) );
  CUDA_ERROR( cudaMemset(f->residuals->Udct, 0,cm->upw * cm->uph * sizeof(int16_t)) );
  CUDA_ERROR( cudaMemset(f->residuals->Vdct, 0,cm->vpw * cm->vph * sizeof(int16_t)) );

  /*
   Used by quantize_block, in dsp.cu    
   */
  //f->mbs[Y_COMPONENT] =
  //(struct macroblock *)calloc(cm->mb_rows * cm->mb_cols, sizeof(struct macroblock));
  //f->mbs[U_COMPONENT] =
  //(struct macroblock *)calloc(cm->mb_rows/2 * cm->mb_cols/2, sizeof(struct macroblock));
  //f->mbs[V_COMPONENT] =
  //(struct macroblock *)calloc(cm->mb_rows/2 * cm->mb_cols/2, sizeof(struct macroblock));
  CUDA_ERROR( cudaMallocManaged(&(f->mbs[Y_COMPONENT]),cm->mb_rows * cm->mb_cols*sizeof(struct macroblock)) );
  CUDA_ERROR( cudaMallocManaged(&(f->mbs[U_COMPONENT]),(cm->mb_rows/2 * cm->mb_cols/2)*sizeof(struct macroblock)   ) );
  CUDA_ERROR( cudaMallocManaged(&(f->mbs[V_COMPONENT]), (cm->mb_rows/2 * cm->mb_cols/2)*sizeof(struct macroblock)) );
  CUDA_ERROR( cudaMemset(f->mbs[Y_COMPONENT], 0,cm->mb_rows*cm->mb_cols*sizeof(struct macroblock)) );
  CUDA_ERROR( cudaMemset(f->mbs[U_COMPONENT], 0,(cm->mb_rows/2 * cm->mb_cols/2)*sizeof(struct macroblock)) );
  CUDA_ERROR( cudaMemset(f->mbs[V_COMPONENT], 0,(cm->mb_rows/2 * cm->mb_cols/2)*sizeof(struct macroblock)) );


  return f;
}

void dump_image(yuv_t *image, int w, int h, FILE *fp)
{
  fwrite(image->Y, 1, w*h, fp);
  fwrite(image->U, 1, w*h/4, fp);
  fwrite(image->V, 1, w*h/4, fp);
}
