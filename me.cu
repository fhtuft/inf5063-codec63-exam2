#include <assert.h>
#include <errno.h>
#include <getopt.h>
#include <limits.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dsp.h"
#include "me.h"

#include <cuda.h> //for cuda magic!

#include "stdcuda.h"




__global__ void cuda_me_block_8x8(uint8_t *orig, uint8_t *ref, int w, int h,
        int range, uint8_t *mv_x, uint8_t *mv_y, uint8_t *predicted)
{
    int mb_x = blockIdx.x;
    int mb_y = blockIdx.y;
    int x = threadIdx.x;
    int y = threadIdx.y;
    int blockId = blockIdx.y*gridDim.x + blockIdx.x;
    //int tid = threadIdx.y*blockDim.x + threadIdx.x;

    int left = mb_x * 8 - range;
    int top = mb_y * 8 - range;
    int right = mb_x * 8 + range;
    int bottom = mb_y * 8 + range;

    /* Make sure we are within bounds of reference frame. TODO: Support partial
        frame bounds. */
    if (left < 0) { left = 0; }
    if (top < 0) { top = 0; }
    if (right > (w - 8)) { right = w - 8; }
    if (bottom > (h - 8)) { bottom = h - 8; }

    int mx = mb_x * 8;
    int my = mb_y * 8;

    int sw = right-left; // search width
    int sh = bottom-top; // search height

    __shared__ int sad_buf[32][32];
    __shared__ int x_buf[32][32];
    __shared__ int y_buf[32][32];
    sad_buf[y][x] = INT_MAX;
    x_buf[y][x] = left + x - mx;
    y_buf[y][x] = top + y - my;

    //dev_sad_block_8x8(orig + my*w+mx, ref + top*w+left, sw, sh, w, sad_buf);
    //uint8_t *block1 = orig + my*w+mx;
    __shared__ uint8_t block1[8][8];
    if (x < 8 && y < 8){
      block1[y][x] = orig[(my + y)*w + (mx + x)];
    }
    __syncthreads();

    uint8_t *window = ref + top*w+left;


    int accum = 0;
    uint8_t *startPos = window + y*w+ x;
    int dx,dy;
    if (x < sw && y < sh){
#pragma unroll
        for (dy=0; dy<8; dy++){
#pragma unroll
	  for (dx=0; dx<8; dx++){
                //accum += abs(block1[dy][dx] - startPos[dy*w+dx]);
	            accum = __usad(block1[dy][dx], startPos[dy*w+dx], accum);
            }
        }
        sad_buf[y][x] = accum;
    }
    __syncthreads();


#define USE_MIN_REDUCE 1
#if USE_MIN_REDUCE
    for (dx=blockDim.x/2; dx>0; dx/=2){
      if (x < dx){
        if (sad_buf[y][x + dx] < sad_buf[y][x]){
          sad_buf[y][x]  = sad_buf[y][x + dx];
          x_buf[y][x] = x_buf[y][x + dx];
          y_buf[y][x] = y_buf[y][x + dx];
        }
      }
      __syncthreads();
    }
    for (dy=blockDim.y/2; dy>0; dy/=2){
      if (x == 0 && y < dy){
        if (sad_buf[y + dy][0] < sad_buf[y][0]){
          sad_buf[y][0] = sad_buf[y + dy][0];
          x_buf[y][0] = x_buf[y + dy][0];
          y_buf[y][0] = y_buf[y + dy][0];
        }
      }
      __syncthreads();
    }

    if (x == 0 && y == 0){
#define PRINT_SAD 0
#if PRINT_SAD
      printf("(%d,%d) sad: %d => mv: (%d,%d)\n", mb_x, mb_y, sad_buf[0][0],
          x_buf[0][0],y_buf[0][0]);
#endif
#undef PRINT_SAD
      mv_x[blockId] = x_buf[0][0];
      mv_y[blockId] = y_buf[0][0];


    	}

    int left_mv = mb_x * 8;
    int top_mv = mb_y * 8;
    int right_mv = left_mv + 8;
    int bottom_mv = top_mv + 8;

                 //int w = cm->padw[color_component];

                 /* Copy block from ref mandated by MV */
    int x_mv, y_mv;

                 //printf("TEST\n");
    if(x < 8 && y < 8) {

    predicted[(y+top_mv)*w+(x+left_mv)] = ref[(y_buf[0][0] + (y+top_mv)) * w + (x_buf[0][0] + (x+left_mv))];

    }



    return;
#else /* USE_MIN_REDUCE */

    //int x, y;
    //printf("(left, top) ~ (%d,%d) ~ (%d,%d)\n", left, top, mx,my);
    int best_sad = INT_MAX;
    if (x == 0 && y == 0){
        for (y = top; y < bottom; ++y)
        {
            for (x = left; x < right; ++x)
            {
                int sad = sad_buf[(y-top)][(x-left)];
#define ME_TEST 0
#if ME_TEST
                int sad2;
                sad_block_8x8(orig + my*8*w+mx, ref + y*w+x, w, &sad2);

                if (sad != sad2){
                    printf("(%d, %d), %d ~ %d\n", x, y, sad, sad2);
                }
#endif
#undef ME_TEST
                if (sad < best_sad)
                {
                    dx = x - mx;
                    dy = y - my;
                    best_sad = sad;
                }
            }
        }
#define PRINT_SAD 0
#if PRINT_SAD
        printf("(%d,%d) sad: %d => mv: (%d,%d)\n", mb_x, mb_y, best_sad,dx,dy);
#endif
#undef PRINT_SAD
        mv_x[blockId] = dx;
        mv_y[blockId] = dy;




    }
#endif /* USE_MIN_REDUCE */
}
/*
4k:
cm->mb_rows = 270 macro blocks
cm->mb_cols = 480 macro blocks
total macro blocks = 129600
foreman:
cm->mb_rows = 36 macro blocks
cm->mb_cols = 44 macro blocks
total macro blocks = 1584
 
*/
void c63_motion_estimate(struct c63_common *cm)
{
  // preparing buffers
  int nRows = cm->mb_rows;
  int nCols = cm->mb_cols;
  int nBlocks = nRows*nCols;

  int8_t *mv_x, *mv_y;
  mv_x = (int8_t*) malloc(nBlocks*sizeof(int8_t));
  mv_y = (int8_t*) malloc(nBlocks*sizeof(int8_t));
  
  uint8_t *dev_mv_x, *dev_mv_y;
  CUDA_ERROR( cudaMalloc(&dev_mv_x, nBlocks*sizeof(int8_t)) );
  CUDA_ERROR( cudaMalloc(&dev_mv_y, nBlocks*sizeof(int8_t)) );
  
  int range = cm->me_search_range;

  // preparing variables for luma
  int w = cm->padw[Y_COMPONENT];
  int h = cm->padh[Y_COMPONENT];
  dim3 blocks = dim3(nCols, nRows);
  dim3 threads = dim3(2*range, 2*range);
  
  cuda_me_block_8x8<<<blocks,threads>>>(cm->curframe->orig->Y,
          cm->refframe->recons->Y, w, h, range, dev_mv_x, dev_mv_y,cm->curframe->predicted->Y);

  CUDA_ERROR( cudaMemcpy(mv_x, dev_mv_x, nBlocks*sizeof(int8_t),
              cudaMemcpyDeviceToHost) );
  CUDA_ERROR( cudaMemcpy(mv_y, dev_mv_y, nBlocks*sizeof(int8_t),
              cudaMemcpyDeviceToHost) );

  /* Compare this frame with previous reconstructed frame */
  int mb_x, mb_y;

#define ME_TEST 0
#if ME_TEST
  /* Luma */
  for (mb_y = 0; mb_y < cm->mb_rows; ++mb_y)
  {
    for (mb_x = 0; mb_x < cm->mb_cols; ++mb_x)
    {
      me_block_8x8(cm, mb_x, mb_y, cm->curframe->orig->Y,
          cm->refframe->recons->Y, Y_COMPONENT);

      struct macroblock *mb =
            &cm->curframe->mbs[Y_COMPONENT][mb_y*w/8+mb_x];

      int8_t tmp_x = mv_x[mb_y*nCols+mb_x];
      int8_t tmp_y = mv_y[mb_y*nCols+mb_x];
      if (tmp_x != mb->mv_x || tmp_y != mb->mv_y){
        printf("Y: (%d, %d), (%d, %d) ~ (%d, %d)\n", mb_x, mb_y, 
                mv_x[mb_y*nCols+mb_x], mv_y[mb_y*nCols+mb_x],
                mb->mv_x, mb->mv_y);
      }
    }
  }
#endif
#undef ME_TEST

  /* copy values from buffer into struct */
  for (mb_y = 0; mb_y < cm->mb_rows; ++mb_y)
  {
    for (mb_x = 0; mb_x < cm->mb_cols; ++mb_x)
    {
      struct macroblock *mb =
            &cm->curframe->mbs[Y_COMPONENT][mb_y*w/8+mb_x];
      mb->mv_x = mv_x[mb_y*nCols+mb_x];
      mb->mv_y = mv_y[mb_y*nCols+mb_x];
      mb->use_mv = 1;
    }
  }

  // preparing variables for chroma
  w = cm->padw[U_COMPONENT];
  h = cm->padh[U_COMPONENT];
  nCols/=2; nRows/=2;
  blocks = dim3(nCols, nRows);
  threads = dim3(range, range);
  
  cuda_me_block_8x8<<<blocks,threads>>>(cm->curframe->orig->U,
          cm->refframe->recons->U, w, h, range/2, dev_mv_x, dev_mv_y,cm->curframe->predicted->U);
 
  cuda_me_block_8x8<<<blocks,threads>>>(cm->curframe->orig->V,
          cm->refframe->recons->V, w, h, range/2, 
          dev_mv_x+nRows*nCols, dev_mv_y+nRows*nCols,cm->curframe->predicted->V);

  CUDA_ERROR( cudaMemcpy(mv_x, dev_mv_x, nBlocks*sizeof(int8_t),
              cudaMemcpyDeviceToHost) );
  CUDA_ERROR( cudaMemcpy(mv_y, dev_mv_y, nBlocks*sizeof(int8_t),
              cudaMemcpyDeviceToHost) );

#define ME_TEST 0
#if ME_TEST
  /* Chroma */
  for (mb_y = 0; mb_y < cm->mb_rows / 2; ++mb_y)
  {
    for (mb_x = 0; mb_x < cm->mb_cols / 2; ++mb_x)
    {
      me_block_8x8(cm, mb_x, mb_y, cm->curframe->orig->U,
          cm->refframe->recons->U, U_COMPONENT);
      me_block_8x8(cm, mb_x, mb_y, cm->curframe->orig->V,
          cm->refframe->recons->V, V_COMPONENT);
    
      struct macroblock *mb =
            &cm->curframe->mbs[U_COMPONENT][mb_y*w/8+mb_x];

      int8_t tmp_x = mv_x[mb_y*nCols+mb_x];
      int8_t tmp_y = mv_y[mb_y*nCols+mb_x];
      if (tmp_x != mb->mv_x || tmp_y != mb->mv_y){
        printf("U: (%d, %d), (%d, %d) ~ (%d, %d)\n", mb_x, mb_y, 
                mv_x[mb_y*nCols+mb_x], mv_y[mb_y*nCols+mb_x],
                mb->mv_x, mb->mv_y);
      }

      mb = &cm->curframe->mbs[V_COMPONENT][mb_y*w/8+mb_x];

      tmp_x = mv_x[(nRows+mb_y)*nCols+mb_x];
      tmp_y = mv_y[(nRows+mb_y)*nCols+mb_x];
      if (tmp_x != mb->mv_x || tmp_y != mb->mv_y){
        printf("V: (%d, %d), (%d, %d) ~ (%d, %d)\n", mb_x, mb_y, 
                tmp_x, tmp_y,
                mb->mv_x, mb->mv_y);
      }
    }
  }
#endif
#undef ME_TEST

  for (mb_y = 0; mb_y < cm->mb_rows / 2; ++mb_y)
  {
    for (mb_x = 0; mb_x < cm->mb_cols / 2; ++mb_x)
    {
      struct macroblock *mb =
            &cm->curframe->mbs[U_COMPONENT][mb_y*w/8+mb_x];
      mb->mv_x = mv_x[mb_y*nCols+mb_x];
      mb->mv_y = mv_y[mb_y*nCols+mb_x];
      mb->use_mv = 1;
      
      mb = &cm->curframe->mbs[V_COMPONENT][mb_y*w/8+mb_x];
      mb->mv_x = mv_x[(nRows+mb_y)*nCols+mb_x];
      mb->mv_y = mv_y[(nRows+mb_y)*nCols+mb_x];
      mb->use_mv = 1;
    }
  }

  free(mv_x);
  free(mv_y);
  CUDA_ERROR( cudaFree(dev_mv_x) );
  CUDA_ERROR( cudaFree(dev_mv_y) );
}


/* Motion compensation for 8x8 block */
 __global__ void cuda_mc_block_8x8(struct c63_common *cm, int mb_x, int mb_y,
    uint8_t *predicted, uint8_t *ref, int color_component)
{
  /*
    curframe: struct frame in cm
    mbs: struct macrblock *mbs in struct frame
    color_component: 0,1,2
    padw: int[color_components:3]
*/
  struct macroblock *mb =
    &cm->curframe->mbs[color_component][mb_y*cm->padw[color_component]/8+mb_x];

  if (!mb->use_mv) { return; }

  int left = mb_x * 8;
  int top = mb_y * 8;
  int right = left + 8;
  int bottom = top + 8;

  int w = cm->padw[color_component];

  // Copy block from ref mandated by MV 
  int x, y;
  
  //printf("bottom: %d  right: %d\n",bottom-top,right-left);
  
  for (y = top; y < bottom; ++y)
  {
    for (x = left; x < right; ++x)
    {
      predicted[y*w+x] = ref[(y + mb->mv_y) * w + (x + mb->mv_x)];
    }
  }
}

// Motion compensation for 8x8 block 
 static void mc_block_8x8(struct c63_common *cm, int mb_x, int mb_y,
    uint8_t *predicted, uint8_t *ref, int color_component)
{
  struct macroblock *mb =
    &cm->curframe->mbs[color_component][mb_y*cm->padw[color_component]/8+mb_x];

  if (!mb->use_mv) { return; }

  int left = mb_x * 8;
  int top = mb_y * 8;
  int right = left + 8;
  int bottom = top + 8;

  int w = cm->padw[color_component];
  //printf("w: %d\n",w);
  
  /* Copy block from ref mandated by MV */
  int x, y;
  
  //printf("bottom: %d  right: %d\n",bottom-top,right-left);
  
  for (y = top; y < bottom; ++y)
  {
    for (x = left; x < right; ++x)
    {
      predicted[y*w+x] = ref[(y + mb->mv_y) * w + (x + mb->mv_x)];
    }
  }
}

/* Motion compensation for 8x8 block */
/*
Tested: Yes
Bugs: Yes, ??
*/
__global__ void cuda_mc_block_8x8(struct c63_common *cm, uint8_t *predicted, uint8_t *ref, int w,int *use_mv,int *mv_x,int *mv_y)
{
  int mb_x = blockIdx.x;
  int mb_y = blockIdx.y;
  

  if (!use_mv[mb_x + mb_y*blockDim.x]) { return; }

  int left = mb_x * 8;
  int top = mb_y * 8;
 

  /*
    (left,top) er kordinatene til høyre hjørne i denne spesiefike mb.
    x og y representerer de globale x og y kordinatene i bilde.
  */
  int x = threadIdx.x + left; 
  int y = threadIdx.y + top;
  /*
    mb_x + mb_y*blockDim.x: Gir posisjonen til data overført til en array av 
    dobble forløkker i c63_motion_compenstate.
  */
  predicted[y*w+x] = ref[(y + mv_y[mb_x + mb_y*blockDim.x] )*w  + (x + mv_x[mb_x + mb_y*blockDim.x])];

  //predicted[y*w+x] = ref[(y + mb->mv_y) * w + (x + mb->mv_x)];
  
}


/*
Tested: Yes
Bugs: Yes, give wrong output, probablu from cuda_mc_block
*/
void c63_motion_compensate_cuda(struct c63_common *cm)
{
  int mb_x, mb_y;

  
  int color_component = Y_COMPONENT;
  struct macroblock *mb;
  int *mv_x,*mv_y; 
  int *use_mv;

  int *use_mv_cuda;
  int *mv_x_cuda,*mv_y_cuda;

  

  CUDA_ERROR(cudaMalloc(&use_mv_cuda,cm->mb_rows*cm->mb_cols*sizeof(int)));
  CUDA_ERROR(cudaMalloc(&mv_x_cuda,cm->mb_rows*cm->mb_cols*sizeof(int)));
  CUDA_ERROR(cudaMalloc(&mv_y_cuda,cm->mb_rows*cm->mb_cols*sizeof(int)));

  use_mv = (int*)malloc(cm->mb_rows*cm->mb_cols*sizeof(int));
  mv_x = (int*)malloc(cm->mb_rows*cm->mb_cols*sizeof(int));
  mv_y = (int*)malloc(cm->mb_rows*cm->mb_cols*sizeof(int));

  /* Luma */
  color_component = Y_COMPONENT; 
  for (mb_y = 0; mb_y < cm->mb_rows; ++mb_y)
  {
    for (mb_x = 0; mb_x < cm->mb_cols; ++mb_x)
    {
      mb = &cm->curframe->mbs[color_component][mb_y*cm->padw[color_component]/8+mb_x];
      
      use_mv[mb_y*cm->mb_cols+mb_x] = mb->use_mv; 
      mv_x[mb_y*cm->mb_cols+mb_x] = mb->mv_x; 
      mv_y[mb_y*cm->mb_cols+mb_x] = mb->mv_y;
    }
  }
  
  CUDA_ERROR( cudaMemcpy((void *)use_mv_cuda,(void *)use_mv,
			 cm->mb_rows*cm->mb_cols*sizeof(int),cudaMemcpyHostToDevice));
  CUDA_ERROR( cudaMemcpy((void *)mv_x_cuda,(void *)mv_x,
			 cm->mb_rows*cm->mb_cols*sizeof(int),cudaMemcpyHostToDevice));
  CUDA_ERROR( cudaMemcpy((void *)mv_y_cuda,(void *)mv_y,
			 cm->mb_rows*cm->mb_cols*sizeof(int),cudaMemcpyHostToDevice));
  
  

  dim3 blocks = dim3(cm->mb_cols, cm->mb_rows);
  dim3 threads = dim3(8,8);
 
 
  cuda_mc_block_8x8<<<blocks,threads>>>(cm,cm->curframe->predicted->Y,cm->refframe->recons->Y,
					cm->padw[Y_COMPONENT],use_mv_cuda,mv_x_cuda,mv_y_cuda);
  CUDA_ERROR( cudaDeviceSynchronize() );
#define MC_TEST 0
#if MC_TEST
  uint8_t *test_outdata;
  int test_x,test_y;
  test_outdata = (uint8_t *)malloc(cm->mb_cols*cm->mb_rows*sizeof(uint8_t));
  for (mb_y = 0; mb_y < cm->mb_rows ; ++mb_y)
  {
    for (mb_x = 0; mb_x < cm->mb_cols ; ++mb_x)
    {
      mc_block_8x8(cm, mb_x, mb_y, test_outdata,cm->refframe->recons->Y, Y_COMPONENT);
    }
  }
  
   for (mb_y = 0; mb_y < cm->mb_rows*8 ; ++mb_y)
  {
    for (mb_x = 0; mb_x < cm->mb_cols*8 ; ++mb_x)
    {
      
      if(test_outdata[mb_x+mb_y*cm->mb_rows*8] != cm->curframe->predicted->Y[mb_x+mb_y*cm->mb_rows*8]) {
	printf("Y(%d,%d)\n",test_outdata[mb_x+mb_y*cm->mb_rows*8],
	       cm->curframe->predicted->Y[mb_x+mb_y*cm->mb_rows*8]);
      }

    }
  } 

  
#endif


  CUDA_ERROR(cudaFree(use_mv_cuda)); 
  CUDA_ERROR(cudaFree(mv_x_cuda)); CUDA_ERROR(cudaFree(mv_y_cuda));

  CUDA_ERROR(cudaMalloc(&use_mv_cuda,(cm->mb_rows/2)*(cm->mb_cols/2)*sizeof(int)));
  CUDA_ERROR(cudaMalloc(&mv_x_cuda,(cm->mb_rows/2)*(cm->mb_cols/2)*sizeof(int)));
  CUDA_ERROR(cudaMalloc(&mv_y_cuda,(cm->mb_rows/2)*(cm->mb_cols/2)*sizeof(int)));


  int cols = cm->mb_cols/2; 
  int rows = cm->mb_rows/2;
  
  /* Chroma U*/
  color_component = U_COMPONENT;
  for (mb_y = 0; mb_y < rows; ++mb_y)
  {
    for (mb_x = 0; mb_x < cols; ++mb_x)
    {
       mb = &cm->curframe->mbs[color_component][mb_y*cm->padw[color_component]/8+mb_x];
       use_mv[mb_y*cols+mb_x] = mb->use_mv; 
       mv_x[mb_y*cols+mb_x] = mb->mv_x; 
       mv_y[mb_y*cols+mb_x] = mb->mv_y;
    }
  }

  CUDA_ERROR( cudaMemcpy((void *)use_mv_cuda,(void *)use_mv,
			 rows*cols*sizeof(int),cudaMemcpyHostToDevice)  );
  CUDA_ERROR( cudaMemcpy((void *)mv_x_cuda,(void *)mv_x,
			 rows*cols*sizeof(int),cudaMemcpyHostToDevice)  );
  CUDA_ERROR( cudaMemcpy((void *)mv_y_cuda,(void *)mv_y,
			 rows*cols*sizeof(int),cudaMemcpyHostToDevice)  );
 

  blocks = dim3(cols,rows);
  threads = dim3(8,8);
  cuda_mc_block_8x8<<<blocks,threads>>>(cm,cm->curframe->predicted->U,cm->refframe->recons->U,
					cm->padw[U_COMPONENT],use_mv_cuda,mv_x_cuda,mv_y_cuda);
  CUDA_ERROR( cudaDeviceSynchronize() );
#if MC_TEST
 for (mb_y = 0; mb_y < rows; ++mb_y)
  {
    for (mb_x = 0; mb_x < cols; ++mb_x)
    {
      mc_block_8x8(cm, mb_x, mb_y, test_outdata,
          cm->refframe->recons->U, U_COMPONENT);  
    }
  }
 
  for (mb_y = 0; rows*8 ; ++mb_y)
  {
    for (mb_x = 0; cols*8 ; ++mb_x)
    {
      if(test_outdata[mb_x+mb_y*rows*8] != cm->curframe->predicted->U[mb_x+mb_y*rows*8]) {
	printf("U(%d,%d)\n",test_outdata[mb_x+mb_y*rows*8],
	       cm->curframe->predicted->U[mb_x+mb_y*rows*8]);
      }
    }
  }
 
#endif


  /* Chroma V*/
  color_component = V_COMPONENT;
  for (mb_y = 0; mb_y < rows; ++mb_y)
  {
    for (mb_x = 0; mb_x < cols; ++mb_x)
    {
       mb = &cm->curframe->mbs[color_component][mb_y*cm->padw[color_component]/8+mb_x];
      
       use_mv[mb_y*cols+mb_x] = mb->use_mv; 
       mv_x[mb_y*cols+mb_x] = mb->mv_x; 
       mv_y[mb_y*cols+mb_x] = mb->mv_y;
    }
  }


  CUDA_ERROR( cudaMemcpy((void *)use_mv_cuda,(void *)use_mv,
			 rows*cols*sizeof(int),cudaMemcpyHostToDevice)  );
  CUDA_ERROR( cudaMemcpy((void *)mv_x_cuda,(void *)mv_x,
			 rows*cols*sizeof(int),cudaMemcpyHostToDevice)  );
  CUDA_ERROR( cudaMemcpy((void *)mv_y_cuda,(void *)mv_y,
			 rows*cols*sizeof(int),cudaMemcpyHostToDevice)  );


  cuda_mc_block_8x8<<<blocks,threads>>>(cm,cm->curframe->predicted->V,cm->refframe->recons->V,
					cm->padw[V_COMPONENT],use_mv_cuda,mv_x_cuda,mv_y_cuda);
  CUDA_ERROR( cudaDeviceSynchronize() );
#if MC_TEST
 for (mb_y = 0; mb_y < rows; ++mb_y)
  {
    for (mb_x = 0; mb_x < cols; ++mb_x)
    { 
      mc_block_8x8(cm, mb_x, mb_y, test_outdata,
          cm->refframe->recons->V, V_COMPONENT);
    }
  }
 printf("TEST2\n");
  for (mb_y = 0; rows*8 ; ++mb_y)
  {
    for (mb_x = 0; cols*8 ; ++mb_x)
    {
      if(test_outdata[mb_x+mb_y*rows*8] != cm->curframe->predicted->V[mb_x+mb_y*rows*8]) {
	printf("V(%d,%d)\n",test_outdata[mb_x+mb_y*rows*8],
	       cm->curframe->predicted->V[mb_x+mb_y*rows*8]);
      }
    }
  }


 free(test_outdata);
#endif
#undef MC_TEST

  free(use_mv);
  free(mv_x);
  free(mv_y);
  
  CUDA_ERROR(cudaFree(use_mv_cuda)); 
  CUDA_ERROR(cudaFree(mv_x_cuda)); CUDA_ERROR(cudaFree(mv_y_cuda));
}


void c63_motion_compensate(struct c63_common *cm)
{
  int mb_x, mb_y;

  /* Luma */
  for (mb_y = 0; mb_y < cm->mb_rows; ++mb_y)
  {
    for (mb_x = 0; mb_x < cm->mb_cols; ++mb_x)
    {
      mc_block_8x8(cm, mb_x, mb_y, cm->curframe->predicted->Y,
          cm->refframe->recons->Y, Y_COMPONENT);
    }
  }

  /* Chroma */
  for (mb_y = 0; mb_y < cm->mb_rows / 2; ++mb_y)
  {
    for (mb_x = 0; mb_x < cm->mb_cols / 2; ++mb_x)
    {
      mc_block_8x8(cm, mb_x, mb_y, cm->curframe->predicted->U,
          cm->refframe->recons->U, U_COMPONENT);
      mc_block_8x8(cm, mb_x, mb_y, cm->curframe->predicted->V,
          cm->refframe->recons->V, V_COMPONENT);
    }
  }
}
