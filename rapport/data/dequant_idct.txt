foreman_4k.yuv, 10 frames.

gprof:
Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total           
 time   seconds   seconds    calls  ms/call  ms/call  name    
 40.91      0.54     0.54  1749600     0.00     0.00  mc_block_8x8
 39.02      1.06     0.52  1944000     0.00     0.00  write_block
  8.33      1.17     0.11 28147773     0.00     0.00  put_bits
  5.30      1.24     0.07 11253926     0.00     0.00  bit_width
  2.27      1.27     0.03  7994956     0.00     0.00  put_byte


nvprof:

==32591== Profiling result:
Time(%)      Time     Calls       Avg       Min       Max  Name
 95.73%  20.9463s        27  775.79ms  21.099ms  2.99121s  cuda_me_block_8x8
  2.36%  515.94ms        30  17.198ms  7.7184ms  61.961ms  cuda_dequant_idct_block_8x8
  1.89%  413.09ms        30  13.770ms  6.3068ms  47.328ms  cuda_dct_quant_block_8x8
  0.03%  5.5179ms        90  61.310us  7.9360us  223.27us  [CUDA memset]

==32591== API calls:
Time(%)      Time     Calls       Avg       Min       Max  Name
 94.76%  20.9494s        39  537.16ms  6.9670us  2.99129s  cudaMemcpy
  4.21%  930.84ms        20  46.542ms  37.814ms  95.587ms  cudaDeviceSynchronize
  0.66%  145.26ms       153  949.44us  8.1650us  129.52ms  cudaMallocManaged
  0.21%  45.701ms        90  507.79us  15.887us  4.2502ms  cudaMemset
  0.12%  27.116ms        87  311.68us  3.1930us  2.7825ms  cudaLaunch
  0.03%  7.2641ms       144  50.445us  6.5230us  131.77us  cudaFree


output:

Encoding frame 0, time ms: 256 Done!
Encoding frame 1, time ms: 2580 Done!
Encoding frame 2, time ms: 2552 Done!
Encoding frame 3, time ms: 2553 Done!
Encoding frame 4, time ms: 2555 Done!
Encoding frame 5, time ms: 2554 Done!
Encoding frame 6, time ms: 2555 Done!
Encoding frame 7, time ms: 2555 Done!
Encoding frame 8, time ms: 2557 Done!
Encoding frame 9, time ms: 2556 Done!
(totally ~23 seconds)


Kommentar:

Her ser vi at vi får redusert cpu-bruken på funksjoner som faktisk
gjør noe med filmen til 1.27 sekunder. Det vil si at vi gjør nesten
alle utregninger på GPU-en, og bruker cpu-en nesten bare til å hente
inn bilder og lagre bilder. Det eneste vi mangler er å få flyttet
motion compensation på GPU-en. Vi gjorde forsøk på det, men det
introduserte bugs. Dermed prioriterte vi å prøve å redusere tiden på
den mest krevende kernelen, "cuda_me_block_8x8".
