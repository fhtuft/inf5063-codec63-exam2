#include <inttypes.h>
#include <math.h>
#include <stdlib.h>

#include "dsp.h"
#include "tables.h"

#include <cuda.h>

#include <stdio.h>

#include "stdcuda.h"
/*
blockDim.x,y,z number of threads in a block in the particular direction
gridDim.x,y,z number of blocks in i grid
threadIdx.x,y,z number given to a particular thread, in a block.
blockIdx,y,z id for a block

 __syncthreads();  sync thread in i block, don't call return or put this in if.

*/


/* CUDA version of transpose, try to use coalesced memory read/write
TESTED: OK
BUGS: none
COMMENTS: 
-Must use 8x8 grid
-Faster(?) thene cuda_transpose_block.           
*/
__global__ void cuda_transpose_block(float *in_data, float *out_data) {
  int tid = threadIdx.x + blockDim.x*threadIdx.y;
  
  __shared__ float cache_in[64];
  __shared__ float cache_out[64];

  cache_in[tid] = in_data[tid];
  __syncthreads();
  
  cache_out[threadIdx.y*8+threadIdx.x] = cache_in[threadIdx.x*8+threadIdx.y];
  __syncthreads();

  
  out_data[tid] = cache_out[tid];
  

}

/* CUDA version of transpose, naive
TESTED: OK
BUGS: None
COMMENTS: -Must use 8x8 grid
*/
__global__ void cuda_transpose_block2(float *in_data, float *out_data) {
  
  out_data[threadIdx.y*8+threadIdx.x] = in_data[threadIdx.x*8+threadIdx.y];
 
}


static void transpose_block(float *in_data, float *out_data)
{
  int i, j;

  for (i = 0; i < 8; ++i)
  {
    for (j = 0; j < 8; ++j)
    {
      out_data[i*8+j] = in_data[j*8+i];
    }
  }
}
/*
Tested:  NO
Bugs: ?
Comments: -må være 64 threads per block, kan være mange blocks
*/
__global__ static void cuda_dtc_id(float *in_data, float *out_data,float **dctlookup) {
  int i = blockIdx.x * blockDim.x + threadIdx.x;
  int j = blockIdx.y * blockDim.y + threadIdx.y;
  int tid = threadIdx.x + blockDim.x*threadIdx.y;
  


  __shared__ float dct[64];
  
  dct[tid] =  in_data[threadIdx.x]*dctlookup[threadIdx.x][threadIdx.y];
  
  //add reduce on 8 
  __syncthreads();
  if(tid%8 <4) {
    dct[tid] += dct[tid+4];
  }
    __syncthreads();
  if(tid%8<2)
    dct[tid] += dct[tid+2];
  __syncthreads();
  if(tid%8 == 0) {
    dct[tid] += dct[tid+1];
    out_data[tid/8] = dct[tid];
  }



}




static void dct_1d(float *in_data, float *out_data)
{
  int i, j;

  for (i = 0; i < 8; ++i)
  {
    float dct = 0;

    for (j = 0; j < 8; ++j)
    {
      dct += in_data[j] * dctlookup[j][i];
    }

    out_data[i] = dct;
  }
}


/*
Each thread calculates it's dct value by lookup in tabel dev_dctlookup.
Add reduce is then done on this values.
Tested:YES
Bugs: none, small diff in float
Note:  Assumes 8x8 threads
*/
static __global__ void cuda_dct_1d_test(float *in_data, float *out_data)
{
	int j = threadIdx.x;
	int i = threadIdx.y;
	int v;
	//printf("(i %d ,j %d )  in_data:%f\n",i,j,in_data[j]);

	__shared__ float m[8][8];

	for(v =0; v <8; v++) {
      
		m[j][i] = in_data[j+8*v] * dev_dctlookup[j][i];
		__syncthreads();
		//printf("m[%d][%d]=%f \n",j,i,m[i][j]);
     
      
		if(j <4) {
			m[j][i] += m[j+4][i];
		}
		__syncthreads();
		if(j <2) {
			m[j][i] += m[j+2][i];
		}
    __syncthreads();
    if(j == 0) {
      m[j][i] += m[j+1][i];
      out_data[i+v*8] = m[0][i];
    }
	}
}




static void idct_1d(float *in_data, float *out_data)
{
  int i, j;

  for (i = 0; i < 8; ++i)
  {
    float idct = 0;

    for (j = 0; j < 8; ++j)
    {
      idct += in_data[j] * dctlookup[i][j];
    }

    out_data[i] = idct;
  }
}
/*
Cuda version of scale_block.
TESTED: OK
BUGS: None
Note:
-Uses 8x8 threads
-Small float diff from cpu
*/
__global__ void cuda_scale_block(float *in_data, float *out_data) {
  
  int tid = threadIdx.x + blockDim.x*threadIdx.y;

  float a = 1.0;
  if(threadIdx.x == 0 || threadIdx.y == 0) {
    a = ISQRT2;
  }
  if(threadIdx.x == 0 && threadIdx.y == 0)
    a = ISQRT2*ISQRT2;
  
  out_data[tid] = in_data[tid]*a;

  
}


static void scale_block(float *in_data, float *out_data)
{
  int u, v;

  for (v = 0; v < 8; ++v)
  {
    for (u = 0; u < 8; ++u)
    {
      float a1 = !u ? ISQRT2 : 1.0f;
      float a2 = !v ? ISQRT2 : 1.0f;

      /* Scale according to normalizing function */
      out_data[v*8+u] = in_data[v*8+u] * a1 * a2;
    }
  }
}


/*
naive cuda version of quantize_block
TESTED: NO
BUGS:
*/
__global__ void cuda_quantize_block(float *in_data, float *out_data, uint8_t *quant_tbl )
{
  
  int zigzag = threadIdx.x + blockDim.x*threadIdx.y; //tid
  
  uint8_t u = dev_zigzag_U[zigzag];
  uint8_t v = dev_zigzag_V[zigzag];
  //uint8_t u = 1,v =1;
  float dct = in_data[v*8+u];
  //float dct = 1.0;
  /* Zig-zag and quantize */
  out_data[zigzag] = (float) roundf((dct / 4.0) / quant_tbl[zigzag]);
  //out_data[zigzag] = ((dct / 4.0) /quant_tbl[1] );

  
}

static void quantize_block(float *in_data, float *out_data, uint8_t *quant_tbl)
{
  int zigzag;

  for (zigzag = 0; zigzag < 64; ++zigzag)
  {
    uint8_t u = zigzag_U[zigzag];
    uint8_t v = zigzag_V[zigzag];

    float dct = in_data[v*8+u];

    /* Zig-zag and quantize */
    out_data[zigzag] = (float) round((dct / 4.0) / quant_tbl[zigzag]);
  }
}

/*
naive cuda version of dequantize_block
TESTED: NO
BUGS: ?
*/
__global__ void cuda_dequantize_block(float *in_data, float *out_data,uint8_t *quant_tbl)
				      
{
  
  int zigzag = threadIdx.x + blockDim.x*threadIdx.y; //tid
  
  uint8_t u = dev_zigzag_U[zigzag];
  uint8_t v = dev_zigzag_V[zigzag];
  
  float dct = in_data[zigzag];
  /* Zig-zag and de-quantize */
  out_data[v*8+u] = (float) roundf((dct * quant_tbl[zigzag]) / 4.0);

}



static void dequantize_block(float *in_data, float *out_data,
    uint8_t *quant_tbl)
{
  int zigzag;

  for (zigzag = 0; zigzag < 64; ++zigzag)
  {
    uint8_t u = zigzag_U[zigzag];
    uint8_t v = zigzag_V[zigzag];

    float dct = in_data[zigzag];

    /* Zig-zag and de-quantize */
    out_data[v*8+u] = (float) round((dct * quant_tbl[zigzag]) / 4.0);
  }
}


__global__ void cuda_dct_quant_block_8x8(uint8_t *in_data, uint8_t *pred, int16_t *out_data,
    uint8_t *quant_tbl)
{
  __shared__ float mb[8][8];
  __shared__ float mb2[8][8];
  __shared__ float tmp[8][8];

  int width = gridDim.x*8;
  unsigned int yPos = blockIdx.y * 8 + threadIdx.y;
  unsigned int xPos = blockIdx.x * 8 + threadIdx.x;
  int datapos = yPos * width + xPos;

  int i = threadIdx.x;
  int j = threadIdx.y;

  //cast int16_t => float
  mb2[j][i] = (int16_t) in_data[datapos] - pred[datapos];
  __syncthreads();

  //for (v = 0; v < 8; ++v) { dct_1d(mb2+v*8, mb+v*8); }
  /* Two 1D DCT operations with transpose */
  int v, n;
#pragma unroll
  for (v = 0; v < 8; ++v){
  	tmp[j][i] = mb2[v][j] * dev_dctlookup[j][i];
  	__syncthreads();

#pragma unroll
  	for (n=4; n>0; n/=2){
  		if (j < n){
  			tmp[j][i] += tmp[j+n][i];
  		}
  		__syncthreads();
		}
	
  	if (j == 0){ 
	  mb[v][i] = tmp[0][i];
  	}
  	__syncthreads();
  }

  //transpose_block(mb, mb2);
  mb2[i][j] = mb[j][i];
  __syncthreads();

  //for (v = 0; v < 8; ++v) { dct_1d(mb2+v*8, mb+v*8); }
#pragma unroll
  for (v = 0; v < 8; ++v){
  	tmp[j][i] = mb2[v][j] * dev_dctlookup[j][i];
  	__syncthreads();
#pragma unroll
  	for (n=4; n>0; n/=2){
  		if (j < n){
  			tmp[j][i] += tmp[j+n][i];
  		}
  		__syncthreads();
    }
	
	
  	if (j == 0){
	  mb[v][i] = tmp[0][i];
  	}

  	__syncthreads();
  }


  //transpose_block(mb, mb2);
  mb2[i][j] = mb[j][i];
  __syncthreads();

  //scale_block(mb2, mb);
	float a1 = !i ? ISQRT2 : 1.0f;
	float a2 = !j ? ISQRT2 : 1.0f;

	/* Scale according to normalizing function */
	mb[j][i] = mb2[j][i] * a1 * a2;
  __syncthreads();
  
  //quantize_block(mb, mb2, quant_tbl);
  int zigzag = j*8 + i;

  uint8_t zig_u = dev_zigzag_U[zigzag];
	uint8_t zig_v = dev_zigzag_V[zigzag];

	float dct = mb[zig_v][zig_u];

	/* Zig-zag and quantize */
	mb2[j][i] = (float) round((dct / 4.0) / quant_tbl[zigzag]);
	__syncthreads();

	datapos = (gridDim.x*blockIdx.y+blockIdx.x)*64 + 8*j+i;
  out_data[datapos] = mb2[j][i];
}

void dct_quant_block_8x8(int16_t *in_data, int16_t *out_data,
    uint8_t *quant_tbl)
{
  float mb[8*8] __attribute((aligned(16)));
  float mb2[8*8] __attribute((aligned(16)));
  
  int i, v;

  for (i = 0; i < 64; ++i) { 
    //printf("in_data[%d]: %d ",i,in_data[i]);
    mb2[i] = in_data[i]; }

  /* Two 1D DCT operations with transpose */
  for (v = 0; v < 8; ++v) { dct_1d(mb2+v*8, mb+v*8); }
  transpose_block(mb, mb2);
  for (v = 0; v < 8; ++v) { dct_1d(mb2+v*8, mb+v*8); }
  transpose_block(mb, mb2);

  scale_block(mb2, mb);
  quantize_block(mb, mb2, quant_tbl);

  for (i = 0; i < 64; ++i) { out_data[i] = mb2[i]; }
}

__global__ void cuda_dequant_idct_block_8x8(int16_t *in_data, uint8_t *pred,
							uint8_t *out_data, uint8_t *quant_tbl)
{
  
  __shared__ float mb[8][8];
  __shared__ float mb2[8][8];
  __shared__ float tmp[8][8];

  int width = gridDim.x*8;
  unsigned int yPos = blockIdx.y * 8 + threadIdx.y;
  unsigned int xPos = blockIdx.x * 8 + threadIdx.x;
  
  
  int i = threadIdx.x;
  int j = threadIdx.y;

  int v,n;

  //for (i = 0; i < 64; ++i) { mb[i] = in_data[i]; }
  int datapos = (gridDim.x*blockIdx.y+blockIdx.x)*64 + 8*j+i;
  mb[j][i] = (float)in_data[datapos];
  __syncthreads();
  
  //dequantize_block(mb, mb2, quant_tbl);
  int zigzag = j*8 + i;

  uint8_t zig_u = dev_zigzag_U[zigzag];
  uint8_t zig_v = dev_zigzag_V[zigzag];
  
  float dct = mb[j][i];
  
  /* Zig-zag and quantize */
  mb2[zig_v][zig_u] = (float)round(dct*quant_tbl[zigzag]/4.0);
  __syncthreads();

  //scale_block(mb2, mb);
  float a1 = !i ? ISQRT2 : 1.0f;
  float a2 = !j ? ISQRT2 : 1.0f;

  /* Scale according to normalizing function */
  mb[j][i] = mb2[j][i] * a1 * a2;
  __syncthreads();

  //for (v = 0; v < 8; ++v) { idct_1d(mb+v*8, mb2+v*8); }
#pragma unroll
  for (v = 0; v < 8; ++v){
  	tmp[j][i] = mb[v][j] * dev_dctlookup[i][j];
  	__syncthreads();
#pragma unroll
  	for (n=4; n>0; n/=2){
  		if (j < n){
  			tmp[j][i] += tmp[j+n][i];
  		}
  		__syncthreads();
  	}
  	if (j == 0){
  		mb2[v][i] = tmp[0][i];
  	}
	__syncthreads();
  }
 
  //transpose_block(mb2, mb);
  mb[i][j] = mb2[j][i];
  __syncthreads();

  //for (v = 0; v < 8; ++v) { idct_1d(mb+v*8, mb2+v*8); }
#pragma unroll
  for (v = 0; v < 8; ++v){
  	tmp[j][i] = mb[v][j] * dev_dctlookup[i][j];
  	__syncthreads();
#pragma unroll
  	for (n=4; n>0; n/=2){
  		if (j < n){
  			tmp[j][i] += tmp[j+n][i];
  		}
  		__syncthreads();
  	}
  	if (j == 0){
  		mb2[v][i] = tmp[0][i];
  	}
	__syncthreads();
  }
  
  //transpose_block(mb2, mb);
  mb[i][j] = mb2[j][i];
  __syncthreads();
  

  datapos = yPos * width + xPos;

  int16_t tmp2 = (int16_t)mb[j][i] + (int16_t)pred[datapos];
  
  if(tmp2 <0) { tmp2 =0 ; }
  else if (tmp2 > 255) { tmp2 = 255; } 
  
  out_data[datapos] = tmp2;


  /* for (i = 0; i < 64; ++i) { out_data[i] = mb[i]; }*/
}




void dequant_idct_block_8x8(int16_t *in_data, int16_t *out_data,
    uint8_t *quant_tbl)
{
  float mb[8*8] __attribute((aligned(16)));
  float mb2[8*8] __attribute((aligned(16)));

  int i, v;

  for (i = 0; i < 64; ++i) { mb[i] = in_data[i]; }

  dequantize_block(mb, mb2, quant_tbl);
  scale_block(mb2, mb);

  /* Two 1D inverse DCT operations with transpose */
  for (v = 0; v < 8; ++v) { idct_1d(mb+v*8, mb2+v*8); }
  transpose_block(mb2, mb);
  for (v = 0; v < 8; ++v) { idct_1d(mb+v*8, mb2+v*8); }
  transpose_block(mb2, mb);

  for (i = 0; i < 64; ++i) { out_data[i] = mb[i]; }
}


void sad_block_8x8(uint8_t *block1, uint8_t *block2, int stride, int *result)
{
  int u, v;

  *result = 0;

  for (v = 0; v < 8; ++v)
  {
    for (u = 0; u < 8; ++u)
    {
      *result += abs(block2[v*stride+u] - block1[v*stride+u]);
    }
  }
}
