/*
Tested:OK
Bugs:none
*/
__global__ void cuda2_sad_block_8x8(uint8_t *block1,uint8_t *block2,int *result) {

  const int numberOfthreads = 64;
  __shared__ int res[numberOfthreads];
  int j;
  int i = threadIdx.x;

  res[i] = abs(block2[i] - block1[i]);
  
  //printf("i:%d  res[i]:%d\n",i,res[i]);
  __syncthreads();
  //add reduction
  for(j = numberOfthreads/2; j != 0 ; j/=2) {
    if(i<j) {
      res[i] +=res[i+j];
   
       //printf("add_red: i:%d res[i]:%d",i,res[i]);
    }
    __syncthreads();
  
  }
  
  
  if(i == 0) {
    *result = res[i];
    //printf("cuda result:%d\n",*result);
  }
  
}

__device__ void dev_sad_block_8x8(uint8_t *block1, uint8_t *window,
        int sw, int sh, int stride, int *sad)
{
  /*
    __shared__ uint8_t cached_block1[64];
    if(tid <64) {
    
    int x = tid/8; int z = tid%8;
    cached_block1[tid] = block1[x*stride + z]; 
    
  }
  */

    int x = threadIdx.x;
    int y = threadIdx.y;
    int accum = 0;
    uint8_t *startPos = window + y*stride + x;
    int dx,dy;
    if (x < sw && y < sh){
        for (dy=0; dy<8; dy++){
            for (dx=0; dx<8; dx++){
                accum += abs(block1[dy*stride+dx] - startPos[dy*stride+dx]);
            }
        }
        sad[y*sw+x] = accum;
    }
}

__global__ void cuda_sad_block_8x8(uint8_t *block1, uint8_t *window,
        int stride, int *sad)
{
  /*
    __shared__ uint8_t cached_block1[64];
    if(tid <64) {
    
    int x = tid/8; int z = tid%8;
    cached_block1[tid] = block1[x*stride + z]; 
    
  }
  */

    int x = threadIdx.x;
    int y = threadIdx.y;
    int accum = 0;
    uint8_t *startPos = window + y*stride + x;
    int dx,dy;
    for (dy=0; dy<8; dy++){
        for (dx=0; dx<8; dx++){
            accum += abs(block1[dy*stride+dx] - startPos[dy*stride+dx]);
        }
    }
    sad[y*blockDim.x+x] = accum;
}

